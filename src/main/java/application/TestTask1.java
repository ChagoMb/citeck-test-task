package application;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Описание задачи: имеется массив чисел, получить список вида {число, количество вхождений числа в массив}, список
 * должен быть отсортирован по количеству вхождений, внутри по возрастания числа. Использовать
 * можно любой алгоритмический язык.
 */
public class TestTask1 {

    public static List<Number> countAllNumbers(int[] ints) {

        Map<Integer, Long> map = Arrays.stream(ints)
                .boxed()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .flatMap(m -> Stream.of(m.getKey(), m.getValue()))
                .collect(Collectors.toList());
    }
}
