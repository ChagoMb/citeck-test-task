package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Описание задачи: имеется таблица с 1 полем, заполненная числами по порядку: {1,2,4,7,8,11..}.
 * Написать SQL Запрос который делает выборку следующего вида (2 столбца): {{3,1},{5,2},...}, Т.е. в
 * первом поле идет число, с которого начинается пропуск, во втором количество пропущенных чисел;
 */
public class TestTask4 {
    private static final String DRIVER_NAME = "org.hsqldb.jdbc.JDBCDriver";
    private static final String URL = "jdbc:hsqldb:mem:testDb;DB_CLOSE_DELAY=-1";

    public static Map<Integer, Integer> selection() {
        Map<Integer, Integer> map = new LinkedHashMap<>();

        Properties properties = new Properties();
        properties.setProperty("driver", DRIVER_NAME);
        try {

            Connection connection = DriverManager.getConnection(URL, properties);

            Statement statement = connection.createStatement();
            statement.execute("CREATE TABLE test (number INT)");
            statement.execute("INSERT INTO test (number) VALUES (1, 2, 4, 7, 8, 11)");

            ResultSet resultSet = statement.executeQuery(
                    "SELECT t1.number + 1, MIN(t2.number) - t1.number - 1 " +
                    "FROM test t1 " +
                        "INNER JOIN test t2 ON t1.number < t2.number " +
                    "GROUP BY t1.number " +
                    "HAVING t1.number < MIN(t2.number) - 1");

            while (resultSet.next()) {
                map.put(resultSet.getInt(1), resultSet.getInt(2));
            }

            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }
}