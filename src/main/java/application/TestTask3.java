package application;

/**
 * Описание задачи: написать алгоритм, как в целом числе самый правый ноль превратить в единицу не используя
 * циклы и рекурсию.
 */
public class TestTask3 {

    public static int increaseByOne(int number) {
        return number < 0 ? -(-number | 1) : number | 1;
    }
}
