package application;

/**
 * Описание задачи: написать алгоритм проверки корректности регулярного выражения, которое включает [,],(,)
 * т.е., например ([][[]()]) - правильно, ([][]()]) – неправильно
 */
public class TestTask2 {

    public static boolean isRight(String regular) {
        char[] tokens = regular.toCharArray();
        int parCount = 0;
        int boxCount = 0;

        if (tokens[0] == ']' || tokens[0] == ')') {
            return false;
        }

        for (int i = 0; i < tokens.length; i++) {
            int index = i;
            if (parCount < 0 || boxCount < 0) {
                break;
            }

            switch (tokens[i]) {
                case '[':
                    ++parCount;
                    break;

                case ']':
                    if (tokens[--index] == '(') {
                        return false;
                    } else {
                        --parCount;
                    }
                    break;

                case '(':
                    ++boxCount;
                    break;

                case ')':
                    if (tokens[--index] == '[') {
                        return false;
                    } else {
                        --boxCount;
                    }
                    break;
            }
        }

        return parCount == 0 && boxCount == 0;
    }
}
