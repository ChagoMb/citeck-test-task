package application;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тесты
 */
public class TestTaskTest {

    /**
     * Проверка первого задания
     */
    @Test
    public void test1() {
        int[] basicArray = new int[] {10, 4, 4, 4, 2, 14, 67, 2, 11, 11, 11, 11, 33, 1, 15, 14};

        String expected = List.of(1, 1, 33, 1, 67, 1, 10, 1, 15, 1, 2, 2, 14, 2, 4, 3, 11, 4).toString();
        String result = TestTask1.countAllNumbers(basicArray).toString();

        assertEquals(expected, result);
    }

    /**
     * Проверка второго задания
     */
    @Test
    public void test2() {
        boolean correct1 = TestTask2.isRight("([][[]()])");
        boolean incorrect1 = TestTask2.isRight("([][]()])");
        boolean incorrect2 = TestTask2.isRight("([(][)])");
        boolean incorrect3 = TestTask2.isRight("](()[])");

        assertTrue(correct1);
        assertFalse(incorrect1);
        assertFalse(incorrect2);
        assertFalse(incorrect3);
    }

    /**
     * Проверка третего задания
     */
    @Test
    public void test3() {
        int expected = -10001;
        int result = TestTask3.increaseByOne(-10000);

        assertEquals(expected, result);
    }

    /**
     * Проверка четвертого задания
     *
     * В методе в таблицу сразу добавляются значения: 1, 2, 4, 7, 8, 11
     * поэтому проверка для конкретно для такого ожидаемого результата: 3=1, 5=2, 9=2
     *
     * Отдельно sql запрос:
     *
     *      SELECT t1.number + 1 AS first_number,
     *             MIN(t2.number) - t1.number - 1 AS count
     *      FROM test t1
     *           INNER JOIN test t2 ON t1.number < t2.number
     *           GROUP BY t1.number
     *           HAVING t1.number < MIN(t2.number) - 1
     *
     */
    @Test
    public void test4() {
        Map<Integer, Integer> result = TestTask4.selection();

        assertEquals("[3=1, 5=2, 9=2]", result.entrySet().toString());
    }
}
